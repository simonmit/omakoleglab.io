---
title: "Visualize dependencies for your codebase with Madge graphs"
description: "Visualize dependencies for your codebase with Madge graphs"
date: 2019-11-01T21:46:50+02:00
cardimage: images/madge/sample.svg
draft: false
---

Lets see some use cases for [madge](https://github.com/pahen/madge), tool to generate nice visualizations for dependencies.

`Madge`: 

> Madge is a developer tool for generating a visual graph of your module dependencies, finding circular dependencies, and give you other useful info.

_Quote from official documentation_

> Create graphs from your CommonJS, AMD or ES6 module dependencies

Works great for both `javascript` and `typescript` projects!

## Simple sample

Sample project structure contains files:
```
- src
|- index.ts
|- lib.ts
|- metrics.ts
|- t.ts
```

Lets install tool itself:
```
$ npm install -g madge
```
To generate graphs [graphviz](http://www.graphviz.org/) need to be installed.

```
$ brew install graphviz
```

And after some time, generate sample:
```
$ madge src/index.js --image sample.svg
```

Generated graph will looks like:
![sample.svg](/images/madge/sample.svg)

Structure is pretty simple and shows that `t.ts` used by all files and `lib.ts` with `metrics.ts` included within `index.ts`

Different colors represent different relations between files:
- `blue`: has dependencies
- `green`: has no dependencies
- `red`: has circular dependencies

# Generate visualization for itself

Lets generate dependency structure for Madge:
```
$ git clone git@github.com:pahen/madge.git
$ cd madge
$ madge bin/cli.js --image madge.svg
```
Generated graph will looks like:
![madge.svg](/images/madge/madge.svg)

Structure is more complicated but perfectly represented within generated image.

# Generate something more complex

One of the `big` javascript projects that comes  into my mind is `aws-sdk`.
Official `SDK` to access various `AWS` cloud services using API calls.

Lets try madge:
```
$ g clone git@github.com:aws/aws-sdk-js.git
$ cd aws-sdk-js
$ madge index.js --image aws-sdk.svg
```
Generated graph will looks like:
![aws-sdk.svg](/images/madge/aws-sdk.svg)

_Visualization is pretty massive and impressive. Make sense to open it as separate tab and zoom in._

Lets visualize `DynamoDB.DocumentClient`:
```
$ cd aws-sdk-js
$ madge lib/dynamodb/document_client.js --image aws-document-client.svg
```
Generated graph will looks like:
![aws-document-client.svg](/images/madge/aws-document-client.svg)

It is now very transparently shown that `core.js`is used almost by every module within codebase.

Also complex relations between modules pretty well shown on diagram. 
This gives possibility to trace file usages and possibly to apply some refactoring or future changes rollout even for such big codebase as `aws-sdk`.

# Conclusion 

`Madge` is a great tool to generate nice and clear visualizations for dependencies 
graph in your projects. For some big codebase it would generate rather complex graph 
which would hard to follow, but gives full overview and represent most commonly used files. 

In my opinion, it is always better to represent relations and flows graphically rather than 
in text form and this tool would be very useful for doing this wherever it is needed.
