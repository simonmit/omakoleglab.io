---
title: Use aws-cdk for creating secretsmanager secret with custom rotation function
description: Sample of aws-cdk stack to generate secret and attach rotate function to update it
date: 2019-12-20T00:00:01+02:00
draft: false
---

# Intro

In software practice it is pretty common to create some secret and use for different use cases. 
Multiple linked services could use it to guarantee data is not compromised by verifying some kind of signature, like [`jwt`](https://jwt.io/introduction/).
In some other cases only one value used to encrypt data and then to decrypt it on receiver.
For this case it is enough to create just one `secret value`.
In current post I would use assumption that only one randomly generated value would be enough. Also sooner or later
secret should be updated to reduce risks of its exposing to 3d parties or even in case of confirmed leak update it very fast.

# Initial setup

There is already docs for basic use of `aws-cdk` [`secretsmanager.Secret`](https://docs.aws.amazon.com/cdk/api/latest/docs/aws-secretsmanager-readme.html)

Lets start by generating basic `cdk` application:

```bash
$ npm install -g aws-cdk
$ mkdir aws-cdk-secrets-rotate
$ cd aws-cdk-secrets-rotate
$ cdk init --language typescript
```
This would generate basic cdk application. And enable to use such commands:
```
* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
```
_for more details check [`aws-cdk` Getting started](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html)_

# Codebase

> _Full post codebase available at [repository](https://gitlab.com/omakoleg/aws-cdk-secrets-rotate)_
> You could download it first and look into with favorite editor

Based on [Overview of the Lambda Rotation Function](https://docs.aws.amazon.com/secretsmanager/latest/userguide/rotating-secrets-lambda-function-overview.html) function should have triggering event with structure:

```ts
interface Event {
    SecretId: string
    ClientRequestToken: string
    Step: 'createSecret' | 'setSecret' | 'testSecret' | 'finishSecret'
}
```
_This type definition specified in Lambda code `lambda/index.ts`_ 

Lambda would be called 4 times for each `Step` value. For current implementation only first step would be 
used `createSecret`. And within implementation newly generated secret would be set with `AWSCURRENT` version.
This would effectively change label for existing key to `AWSPREVIOUS`.

## Cdk stacks

Stack in  `lib/aws-cdk-secrets-rotate-stack.ts` would have such structure:

```ts
import cdk = require('@aws-cdk/core');
import { Duration } from '@aws-cdk/core';
import { Secret, } from '@aws-cdk/aws-secretsmanager';
import { Function, Runtime, Code } from '@aws-cdk/aws-lambda';
import { ServicePrincipal, PolicyStatement } from '@aws-cdk/aws-iam';
import { join } from 'path';

export class AwsCdkSecretsRotateStack extends cdk.Stack {
  private readonly secretName = "my-secret-name-here";
  private readonly keyInSecretName = 'keyInSecret';

  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const secret = new Secret(this, 'Secret', {
      description: "My Secret",
      secretName: this.secretName,
      generateSecretString: {
        secretStringTemplate: JSON.stringify({}),
        generateStringKey: this.keyInSecretName,
      }
    });
    const lambda = new Function(this, 'LambdaSecretRotate', {
      functionName: 'lambda-keys-rotate',
      runtime: Runtime.NODEJS_12_X,
      handler: 'index.handler',
      logRetention: 3,
      timeout: Duration.minutes(10),
      code: Code.fromAsset(join(__dirname, '..', 'lambda')),
      environment: {
        REGION: cdk.Stack.of(this).region,
        SECRET_NAME: this.secretName,
        KEY_IN_SECRET_NAME: this.keyInSecretName
      }
    })
    secret.addRotationSchedule('RotationSchedule', {
      rotationLambda: lambda,
      automaticallyAfter: Duration.hours(24)
    });
    secret.grantRead(lambda);
    lambda.grantInvoke(new ServicePrincipal('secretsmanager.amazonaws.com'))
    lambda.addToRolePolicy(new PolicyStatement({
      resources: [secret.secretArn],
      actions: ['secretsmanager:PutSecretValue']
    }));
  }
}
```
This is full CDK code used. Main Resources:

1) `Secret` - would create new secret value using name `secretName`. And by Providing `generateSecretString` it would
generate initial `json` structure with one additional key using value from `keyInSecretName`.
In `secretStringTemplate` specified just empty json `'{}'` so initially secret would looks like this:

```json
{
    "keyInSecret":"<some random string>" 
}
```
But immediately after keys rotation Lambda resource created, it would be executed and receive value from function implementation:
```json
{"keyInSecret":"d2a74cd6fa4162ec6eccf75fdf281745a9ca3a8c5f7c736937b9ef360b56c7ef"}
```
2) `Function` (`LambdaSecretRotate`) would represent attached rotation lambda with implementation from directory `./lambda`.
Except pretty common attributes few environment variables passed into:
```ts
environment: {
    REGION: cdk.Stack.of(this).region,
    SECRET_NAME: this.secretName,
    KEY_IN_SECRET_NAME: this.keyInSecretName
}
```
To determine what key to update and what key within SecretString json to update.

3) `RotationSchedule` Would instruct to use specified Lambda and run it every `24` hours

4) Necessary roles to allow SecretsManager to call Lambda and Lambda to update secret value

```ts
secret.grantRead(lambda);
```
_Allow lambda to read secret value_
```ts
lambda.grantInvoke(new ServicePrincipal('secretsmanager.amazonaws.com'))
```
_Allow secretsmanager to invoke Lambda_
```ts
lambda.addToRolePolicy(new PolicyStatement({
    resources: [secret.secretArn],
    actions: ['secretsmanager:PutSecretValue']
}));
```
_Allow Lambda to update secret value_

Last role wiring is missing but necessary to apply changes to secret key.

## Cdk application

Application in  `bin/aws-cdk-secrets-rotate.ts` would have structure:

```ts
import 'source-map-support/register';
import cdk = require('@aws-cdk/core');
import { AwsCdkSecretsRotateStack } from '../lib/aws-cdk-secrets-rotate-stack';

const app = new cdk.App();
new AwsCdkSecretsRotateStack(app, 'AwsCdkSecretsRotateStack');

```
_Just one stack created from previous section_

## Lambda Code

`typescript` Source code available in `./lambda`. It would be compiled in `javascript` using generated 
`aws-cdk` application `tsc` compiler, according to config `tsconfig.json`.

Lambda Code:
```ts
import { randomBytes } from 'crypto';
import { SecretsManager } from 'aws-sdk'

const secretName = process.env.SECRET_NAME!;
const keyInSecret = process.env.KEY_IN_SECRET_NAME!;
const secretsManager = new SecretsManager({
    region: process.env.REGION || 'eu-west-1'
})
interface Event {
    SecretId: string
    ClientRequestToken: string
    Step: 'createSecret' | 'setSecret' | 'testSecret' | 'finishSecret'
}
export async function handler(event: Event) {
    if (event.Step === 'createSecret') {
        await secretsManager
            .putSecretValue({
                SecretId: secretName,
                SecretString: JSON.stringify({
                    [keyInSecret]: randomBytes(32).toString('hex')
                }),
                VersionStages: ['AWSCURRENT']
            })
            .promise()
    }
}
```
First environment variables read prom `process.env.*`: `SECRET_NAME`, `KEY_IN_SECRET_NAME`, `REGION` (used to create instance of `SecretsManager`).

Interface `Event` describes triggering event structure.

`handler` function has one useful processing attached to `createSecret` Step. Within it generated new secret using 
`randomBytes(32).toString('hex')` (ps. Secret length would be `64`).
Then it saved into `SECRET_NAME` with `AWSCURRENT` version which would make it available for use.

# Deploy application

```bash
$ yarn build && yarn test
yarn run v1.19.1
$ tsc
✨  Done in 8.17s.
yarn run v1.19.1
$ jest
 PASS  test/aws-cdk-secrets-rotate.test.ts
  AlertingStack
    ✓ Contains ::Secret (84ms)
    ✓ Contains ::RotationSchedule
    ✓ Contains Lambda ::Role
    ✓ Contains ::Policy for key access
    ✓ Contains ::Function (1ms)
    ✓ Contains LogRetention
    ✓ Contains permission to call function by secretsmanager

Test Suites: 1 passed, 1 total
Tests:       7 passed, 7 total
Snapshots:   0 total
Time:        1.585s, estimated 5s
Ran all test suites.
✨  Done in 2.23s.
```

Deploy application:
```sh
$ yarn cdk deploy
```
## Check functionality

When CDK application deployed secret `my-secret-name-here` would be available immediately with some randomly
generated value. After some short period of time Lambda would be executed and secret would receive 
final structure:
```sh
$ aws secretsmanager get-secret-value --secret-id my-secret-name-here
{
    "Name": "my-secret-name-here", 
    "VersionId": "7b36c954-5184-42dd-85ed-f69db8674fe4", 
    "SecretString": "{\"keyInSecret\":\"d2a74cd6fa4162ec6eccf75fdf281745a9ca3a8c5f7c736937b9ef360b56c7ef\"}", 
    "VersionStages": [
        "AWSCURRENT"
    ], 
    "CreatedDate": ..., 
    "ARN": "arn:...."
}
```
_Current version is `7b36c954-5184-42dd-85ed-f69db8674fe4`_

Open SecretsManager AWS console and trigger rotation function manually for our secret.

Check updated value:
```sh
$ aws secretsmanager get-secret-value --secret-id my-secret-name-here
{
    "Name": "my-secret-name-here", 
    "VersionId": "c05f4ef0-4d8b-441a-8c94-43448be43ea3", 
    "SecretString": "{\"keyInSecret\":\"34a73aba398560d27cf54771580e7e6c39581eb7985ce1d00fbed7e44835c0c3\"}", 
    "VersionStages": [
        "AWSCURRENT"
    ], 
    "CreatedDate": ..., 
    "ARN": "arn:..."
}
```
_Updated version is `c05f4ef0-4d8b-441a-8c94-43448be43ea3`_

Also `describe-secret` could show more information:
```sh
$ aws secretsmanager describe-secret --secret-id my-secret-name-here
{
    "Name": "my-secret-name-here", 
    "VersionIdsToStages": {
        "7b36c954-5184-42dd-85ed-f69db8674fe4": [
            "AWSPREVIOUS"
        ], 
        "c05f4ef0-4d8b-441a-8c94-43448be43ea3": [
            "AWSCURRENT"
        ]
    }, 
    "Tags": [
        ...
    ], 
    "RotationRules": {
        "AutomaticallyAfterDays": 1
    }, 
    ....
}
```

# Conclusion 

`aws-cdk`  enables developers to build applications much faster. Common application could be shared
with ease by using separate stacks with all required resources. 
Comparing to `yaml` / `json` templates `typescript` CDK gives much more nice dev experience with syntax highlighting and autocompletion.
Also type definitions could suggest what values available for each kind of resources with documentation included.
SecretsManager resource is not exception and available via `@aws-cdk/aws-secretsmanager` package. 
Using it makes much better experience when creating `secret` and custom function to rotate it.
