---
title: OneDrive React Application deployed to gitlab pages
description: Notes about building typescript React OneDrive SPA with oAuth token auth flow.
date: 2019-12-25T00:00:01+02:00
cardimage: images/react-onedrive/onedrive-app.png
draft: false
---
# Intro

Building and hosting Single Plage Applications (SPA) nowadays became very easy and fast. Most of popular
code hosting providers provide this functionality out of the box (like GithubPages, GitlabPages, Bitbucket Cloud etc)
In this short tutorial would be explained how:

- Build sample React typescript application with `create-react-app`
- Deploy React application to GitlabPages
- Use Token oAuth flow for SPA using Microsoft graph API.
- List folders in OneDrive root

# Impatient section

Code available at [https://gitlab.com/omakoleg/react-onedrive](https://gitlab.com/omakoleg/react-onedrive)

Application deployed at [https://omakoleg.gitlab.io/react-onedrive](https://omakoleg.gitlab.io/react-onedrive)

> Visiting page would require to allow OneDrive files Read.

# Doing some setup work

## Gitlab account and repository

Create new account [here](https://gitlab.com/users/sign_in#register-pane) and create new repository.
In my case newly created repository is `react-onedrive`. 
This path would be referenced multiple times within this post, you could replace it with your own.
Repository could be private for this sample.
For this post static website would have path `https://omakoleg.github.io/react-onedrive`.

## Microsoft account and application registration

To be able to access OneDrive API new application is required.
Proceed to [registration](https://azure.microsoft.com/en-us/account/)
And after open Applications [management](https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade)

Create `New Registration`. In my case application called `React-OneDrive`.

![Application](/images/react-onedrive/onedrive-app.png)

Where:
 - `A` Application Id (some `uuid`), it would be needed later in codebase.
 - `B` Redirect Urls. 
 - `C` API Permissions.

 Regarding `B`. There should be 2 redirect urls allowed. One for local development server `http://locahost:3000`.
 It is `http` !!! And second `https://omakoleg.github.io/react-onedrive` for hosted with Gitlab pages.
 Both redirect urls would navigate to main application page.

 Also `Access Token` should be allowed for SPA without backend.

 ![Callback](/images/react-onedrive/onedrive-callback.png)

 For API permissions `C` there should be set `Files.Read` (allow files read) only. This would be enough for sample.

 ![Api Permissions](/images/react-onedrive/onedrive-permissions.png)

## Generate Application with react-scripts

Navigate to some directory. And execute `create-react-app` without installation:

```bash
$ npx create-react-app react-onedrive --template typescript
```
This would create application in `react-onedrive` folder.

Attach gitlab remote ro repository:
```bash
$ cd react-onedrive
$ git remote add origin git@gitlab.com:omakoleg/react-onedrive.git
```
Run fresh application:
```bash
$ yarn start
```
Browser would open `http://localhost:3000` automatically. 

Check generated `README.md` within project or even read about more options available for `create-react-app`
[here](https://create-react-app.dev/docs/getting-started)

# OneDrive Application

There would be hash part of Url used in application by authentication redirect.
In usual SPA applications hash part of url is also used by `react-router`.
To make sure there is NO overlap problems React Router would be also added.

```bash
$ yarn add react-router-dom @types/react-router-dom
```
In application there would be 2 pages: `Home` and `About`. 
More info about [`react-router`](https://create-react-app.dev/docs/adding-a-router/). 
Pages structure also shown in `Basic Sample` in documentation page.

Application configuration would be provided from `src/lib/config.ts`:
```ts
export const config: Config = {
    localStorageTokenName: 'access-token',
    // PUBLIC_URL would allow to switch environments with default fallback.
    siteUrl: process.env.PUBLIC_URL || "http://localhost:3000",
    oneDrive: {
        appId: "<Application ID from API application setup step, check A>",
        scope: 'Files.Read',
        baseUrl: "https://graph.microsoft.com/v1.0"
    }
};
export interface Config {
    localStorageTokenName: string;
    siteUrl: string;
    oneDrive: {
        appId: string;
        scope: string;
        baseUrl: string;
    }
}
```
_Check more info regarding `PUBLIC_URL` behavior [here](https://create-react-app.dev/docs/adding-custom-environment-variables/#referencing-environment-variables-in-the-html)_

## Authentication

For OneDrive API `oAuth token flow` would be used. Read more on official [page](https://docs.microsoft.com/en-us/onedrive/developer/rest-api/getting-started/msa-oauth?view=odsp-graph-online#token-flow).
In overall authentication would have redirect to Microsoft auth page, allowing application to use `Files.Read` permission 
for this application and redirecting back to main application page with `access_token` within redirect url hash.
After allowing access there would be also email notification.
Token passed in hash because it is intended to be used in SPA only within browser and never should be given to backend,
 which actually doesn't exist for current application. 
There are few more limitations when using token flow. First access_token provided is short lived and would require application 
to request new one in some time. And second limitation is that refresh_token not given at all and updating access_token would 
require few manual clicks again. Later for more advanced use case, `oAuth code flow` could be used. But for simplicity I would stick to token flow.

Modify entry point `src/index.tsx` to include authentication page:
```ts
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { forceAuth } from './lib/auth';
forceAuth(); // <-- would force check auth
ReactDOM.render(<App />, document.getElementById('root'));
```
This would provide auth check on every application load.

Create `src/lib/auth.ts` with contents:
```ts
import { config } from '../lib/config';

/*eslint no-restricted-globals: ["error", "event"]*/
export const forceAuth = () => {
    const parts = location.hash.split("&");
    if (parts.length > 0) {
        let partsMap = parts.map(i => i.split("="));
        let accessToken = partsMap.find(i => i[0] === '#access_token');
        if (accessToken) {
            localStorage.setItem(config.localStorageTokenName, accessToken[1]);
            window.location.href = config.siteUrl;
        }
    }
    const token = localStorage.getItem(config.localStorageTokenName)
    if (!token) {
        window.location.href = authUrl;
    }
}

export const mergeAuthHeader = (headersObject: any) => {
    headersObject["Authorization"] = "Bearer " + localStorage.getItem(config.localStorageTokenName);
    return headersObject;
}

export const logout = () => {
    localStorage.removeItem(config.localStorageTokenName);
    window.location.href = authUrl;
}

export const authUrl =
    [
        "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=" + config.oneDrive.appId,
        "scope=" + config.oneDrive.scope,
        "response_type=token",
        "redirect_uri=" + config.siteUrl
    ].join('&');

```

Auth functions:
1) `forceAuth` Would check for `access_token` within url hash first of all. 
If it is set, parse it and set into local storage key `config.localStorageTokenName` 
and redirect to `config.siteUrl` to clean current url. Redirect is not necessary but clean url looks nicer.
After, it would read token out of local storage and if it is not set, would redirect to `authUrl` built 
from configuration.

Function cover such auth cases:
- Unauthorized initial page visit. No token found, redirect to auth.
- Redirect from auth, set token to local storage, redirect to main page.
- Visit page with token in local storage, do nothing and continue rendering page.

> Be aware that in case of implementation error, this function could produce infinite redirect loop, be careful !!

2) `mergeAuthHeader` would be used in each API request to OneDrive and attach `access_token` into correct header.

3) `logout` to force remove token from local storage and redirect to authentication. 
Application is not able to work without auth so redirect would happen always.

Now reload browser or execute:
```sh
$ yarn start
```
To see browser redirect to Microsoft login page and going back to application.

## OneDrive API

Update main `src/App.tsx` component:

```tsx
import React, { useState, useEffect } from 'react';
import { getChildren, OneDriveItem } from './lib/onedrive';
import { HashRouter, Link, Switch, Route } from 'react-router-dom';

const App = () =>
  <HashRouter>
    <div>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
      </ul>
      <hr />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
      </Switch>
    </div>
  </HashRouter>

export default App;

function Home() {
  const [folders, setFolders] = useState<OneDriveItem[]>([]);
  const [error, setError] = useState(undefined);

  useEffect(() => {
    getChildren()
      .then(setFolders)
      .catch(e => setError(e.toString()));
  }, []);

  return (
    <div style={{ padding: '50px' }}>
      <p>One Drive application</p>
      {error &&
        <div style={{ color: 'red' }}>Error occurred: {error}</div>
      }
      <div>
        {folders && folders.map((folder, idx) => {
          return <div key={idx}>
            {folder.name}
          </div>
        })}
      </div>
    </div>
  );
}

const About = () =>
  <div>
    <h2>About</h2>
  </div>

```

`App` has `HashRouter` and 2 pages represented by components `Home` and `About`.

Within  `App` there is call to function `getChildren` which provides OneDrive data.

`folders` and `error` represent current page variables to render Top level directory structure or error.

`getChildren` implemented in `src/lib/onedrive.ts`:

```ts
import { config } from "./config";
import { mergeAuthHeader, logout } from "./auth";

export interface OneDriveItem {
    [k: string]: any;
    name: string;
}

async function doRequest(fullUrl: string, options: any) {
    options.headers = mergeAuthHeader(options.headers || {});
    const response = await fetch(fullUrl, options)
    if (response.status === 401) {
        logout();
        throw Error("Unauthorized error");
    }
    return response.json();
}

export async function getChildren(): Promise<OneDriveItem[]> {
    const path = "/me/drive/root/children";
    const response = await doRequest(config.oneDrive.baseUrl + path, {
        method: "get"
    })
    return response.value;
}
```
> To not go into much details about OneDrive API responses, interface `OneDriveItem` would fix only `name` parameter.
> For more information check OneDrive [API docs](https://docs.microsoft.com/en-us/onedrive/developer/rest-api/api/driveitem_list_children?view=odsp-graph-online) for getting Items in folder

`doRequest` would attach authentication with `mergeAuthHeader` to request header and check for `401 response.status` in case of
wrong / missing auth in request. Also when no errors happened would decode response payload `response.json()`.

`getChildren` would use `doRequest` with `GET` method and take `.value` key from response.

Implementation based on `async/await` so no need to worry about exceptions handling, they would ba handled in 
`src/App.tsx` on line 37:
```ts
useEffect(() => {
    getChildren()
      .then(setFolders)
      .catch(e => setError(e.toString()));
  }, []);
```

> Note: When `401` received from API, `logout` would be called and immediately thrown `Error("Unauthorized error")`. 
> It would show error on a page and perform redirect.

Now again reload browser or execute:
```sh
$ yarn start
```
To see browser redirect to Microsoft login page. 
After logging in it would redirect back and show list of top level folders stored in Yours `OneDrive`. 
Same as shown in official OneDrive UI [here](https://onedrive.live.com/)

Currently application is fully functional but only on local computer. Works `100%` :)

## Deploy gitlab pages

SPA Application to Gitlab Pages is pretty easy. Read more in [documentation](https://gitlab.com/help/user/project/pages/index.md)
if some more clarifications needed.

Create `.gitlab-ci.yml` file in repository root.

```yaml
image: node:12

pages:
  stage: deploy
  script:
  - yarn install --frozen-lockfile
  - NODE_ENV=production PUBLIC_URL="https://omakoleg.gitlab.io/react-onedrive" yarn build
  - rm -rf public/
  - mv build/ public/
  artifacts:
    paths:
    - public
  only:
  - master

```

Steps:
1) Install yarn dependencies based on `yarn.lock`

2)  Execute `yarn build` which would invoke `react-scripts` and prepare production build of React Application.
Important here to provide `PUBLIC_URL`.
It is used in `src/lib/config` to determine public url to website `siteUrl: process.env.PUBLIC_URL || "http://localhost:3000"`.
Assets and redirects would be based on this url.

3) Gitlab pages require to have `public` so after `yarn build`, remove `public` folder. And move build artifacts from `build` into `public`

> All other steps would be done by Gitlab because of `pages` step in `yaml`.

```sh
$ git add -A
$ git commit -m 'Final application'
$ git push origin master
```

Navigate to `https://gitlab.com/omakoleg/react-onedrive/pages` (possibly another url in your case). 

Or open `Settings -> pages` in your repository.

![Gitlab pages setup](/images/react-onedrive/gitlab-pages.png)

After some time page would be available via provided url.

Navigate to [https://omakoleg.gitlab.io/react-onedrive](https://omakoleg.gitlab.io/react-onedrive) to see Application in action.

# Cleanup

When Microsoft auth page opened, it asked about permission to access your files.

You could revoke it [here](https://account.live.com/consent/Manage)

Find application by Name, click Edit and click `Remove these permissions`.

Also after development finished I would recommend to remove allowed redirect to `http://localhost:3000` in 
API Application setup described in section `Microsoft account and application registration -> B`. 
On this [page](https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade)

# Conclusion

Creating SPA's is pretty easy with help of `create-react-app` template and Gitlab static pages. 
Access to OneDrive API is nicely documented and give mush more possibilities shown in this post.
And as usual `typescript` enable developers to work faster on solutions. 

So You should consider to try and build your own SPA today )
