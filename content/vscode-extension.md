---
title: "Building VSCode extension"
description: "Building VSCode extension to highlight outdated npm packages with CodeLens"
date: 2019-11-20T00:00:01+02:00
cardimage: images/vscode-extension/correct_versions_only.png
draft: false
---

# Intro

In this post I would explain how to build VSCode extension.
Sample extension would be checking `npm` package versions provided in `package.json`. And give hints what is the 
most recent version of this package in registry.
Would start by provided official bootstrap documentation and go beyond of it.
Extension is written in `typescript` and use `npmjs.com` for checking most recent package version.

# Bootstrapping codebase

Extension code hosted at gitlab repository [vscode-extension-version-me](https://gitlab.com/omakoleg/vscode-extension-version-me)

Would recommend to download it first and open in your favorite `VSCode` editor `:)`

_To see results of this section without typing in: `git checkout 0_initial`_

Extension named as `version-me`. We would use it later.

Based on officially provided [Your First Extension](https://code.visualstudio.com/api/get-started/your-first-extension) steps 
lets start:
```
npm install -g yo generator-code
```
and then 
```
yo code
```
With some inputs given:
{{< highlight txt >}}
? What type of extension do you want to create? New Extension (TypeScript)
? What's the name of your extension? version-me
? What's the identifier of your extension? version-me
? What's the description of your extension? Show recent versions
? Initialize a git repository? Yes
? Which package manager to use? yarn
{{< /highlight >}}

We would have basic extension generated:
{{< highlight txt >}}
$ cd version-me
$ ls -l
-rw-r--r--    1 a  b    237 Nov 19 15:50 CHANGELOG.md
-rw-r--r--    1 a  b   2057 Nov 19 15:50 README.md
drwxr-xr-x  105 a  b   3360 Nov 19 15:50 node_modules
-rw-r--r--    1 a  b    835 Nov 19 15:50 package.json
drwxr-xr-x    4 a  b    128 Nov 19 15:50 src
-rw-r--r--    1 a  b    584 Nov 19 15:50 tsconfig.json
-rw-r--r--    1 a  b    248 Nov 19 15:50 tslint.json
-rw-r--r--    1 a  b   2616 Nov 19 15:50 vsc-extension-quickstart.md
-rw-r--r--    1 a  b  33903 Nov 19 15:50 yarn.lock
{{</ highlight >}}

Of course open in with VScode: `code .`

So far main extension codebase is in `src/extension.ts` (without comments):

{{< highlight ts >}}
import * as vscode from 'vscode';

export function activate(context: vscode.ExtensionContext) {
	console.log('Congratulations, your extension "version-me" is now active!');
	let disposable = vscode.commands.registerCommand('extension.helloWorld', () => {
		vscode.window.showInformationMessage('Hello World!');
	});
	context.subscriptions.push(disposable);
}
export function deactivate() {}
{{< /highlight >}}
_Sample generated extension registers one command and when executed shows information message_

Lets try to run it. In `VSCode` open Debug view by `Shift+Cmd+D`.
Click `Ctrl+F5` to run build without debugging.

New window should appear. Open command prompt by `Shift+Cmd+P` and type or select `Hello World`.
Message in right bottom corner should appear. This is all what our freshly generated extension could do.
From my point of view official documentation described process of extension bootstrapping very well!

# Prepare for our use case

_To see results of this section without typing in: `git checkout 1_workspace`_

We would be running our code sample pretty ofter so it would be nice to tweak our extension 
to start within some predefined text directory with already prepared testing `.json` files.

Within `src/test` create new folder `workspace` and create within folder 3 files with some sample 
package structure and some outdated package versions. So far we would use only numeric versions 
(no hash versions and relative paths)

(1) `package.json`:
{{< highlight json>}}
{
	"name": "test-package",
	"displayName": "test",
	"description": "Test extension sample",
	"version": "0.0.1",
	"devDependencies": {
		"@types/glob": "^7.1.1",
		"@types/mocha": "^5.2.7",
		"@types/node": "^12.11.7",
		"@types/vscode": "^1.40.0",
		"glob": "^7.1.5",
		"mocha": "^6.2.2",
		"typescript": "^3.6.4",
		"tslint": "^5.20.0",
		"vscode-test": "^1.2.2"
	},
	"dependencies": {
		"co-famo": "0.1"
	}
}
{{</ highlight>}}

And (2) `another.json`:
{{< highlight json>}}
{
	"name": "test-package",
	"displayName": "test",
	"description": "Test extension sample",
	"version": "0.0.1",
	"devDependencies": {
		"@types/glob": "7.1.5",
		"glob": "6.1.5",
		"aws-sdk": "^1.1.1"
	},
	"dependencies": {
		"co-famo": "0.1"
	}
}

{{</ highlight>}}

And finally useless file (3) `index.ts`:
{{< highlight ts>}}
export const main = () => console.log("I am useless");

{{</ highlight>}}
_Just to be sure it would not be used by extension_

Now folder would contain 2 files like this:

{{< highlight zsh>}}
➜  version-me git:(master) cd src/test/workspace 
➜  workspace git:(master) ✗ ls -la
total 16
-rw-r--r--  1 a  b  249 Nov 19 17:13 another.json
-rw-r--r--  1 a  b  412 Nov 19 17:12 package.json
{{</ highlight >}}

Within our extension startup configuration file `.vscode/launch.json` we need 
to specify this folder as path which would be used on `Run` step (line 8 added):

{{<highlight json "hl_lines=8">}}
{
    "name": "Run Extension",
    "type": "extensionHost",
    "request": "launch",
    "runtimeExecutable": "${execPath}",
    "args": [
        "--extensionDevelopmentPath=${workspaceFolder}",
        "${workspaceFolder}/src/test/workspace"
    ],
    "outFiles": [
        "${workspaceFolder}/out/**/*.js"
    ],
    "preLaunchTask": "${defaultBuildTask}"
}
{{</highlight>}}

Run our extension: `Shift+Cmd+D` (switch to debug view), `Ctrl+F5` (run)

![workspace](/images/vscode-extension/1_workspace.png)

It would be opened within our `workspace` folder and show test files.

# Access files within extension

_To see results of this section without typing in: `git checkout 2_sample_lense`_

Next, for each `.json` file we need to be able to add some hints about expired versions.
For this we could use already provided by VSCode [`CodeLens`](https://code.visualstudio.com/api/references/vscode-api#CodeLens)
and in particular [`CodeLensProvider`](https://code.visualstudio.com/api/references/vscode-api#CodeLensProvider)
with custom implementation

Within `src` create file `VersionsCodeLensProvider.ts`:

{{< highlight ts >}}
import { CodeLensProvider, TextDocument, CodeLens, Range, Position, Command } from "vscode";

export class VersionsCodeLensProvider implements CodeLensProvider {
    // Each provider requires a provideCodeLenses function which 
    // will give the various documents the code lenses
    provideCodeLenses(document: TextDocument): Promise<CodeLens[]> {
        let sourceCode = document.getText()
        const results: Promise<CodeLens>[] = [];
        const sourceCodeArr = sourceCode.split('\n');
        for (let lineNum = 0; lineNum < sourceCodeArr.length; lineNum++) {
            results.push(this.getCodeLenseForLine(lineNum, sourceCodeArr[lineNum]));
        }
        return Promise.all(results);
    }

    // Function to generate CodeLens for each code line
    getCodeLenseForLine(lineNum: number, line: string): Promise<CodeLens> {
        const addedMessage = `Added line`
        let range = new Range(
            new Position(lineNum, 0),
            new Position(lineNum, addedMessage.length - 1)
        )
        // Define what command we want to trigger when activating the CodeLens
        let c: Command = {
            command: "extension.fixVersion",
            title: addedMessage,
        };
        return Promise.resolve(new CodeLens(range, c))
    }
}
{{</ highlight>}}

Our custom `CodeLensProvider` should implement function `provideCodeLenses(document: TextDocument)` which returns 
`CodeLens[]` or Promise with same resulting type: `Promise<CodeLens[]>`.
For now, we just need to be sure that out code will add anything to each line.
For ach line function `getCodeLenseForLine` called with 2 parameters: line number, and line contents.

Within `getCodeLenseForLine` new `CodeLens` instance generated which itself require:

 * (1) lines range where it should appear

 * (2) command to be executed when `CodeLens` text is clicked.

Regarding range (1) everything pretty clear. It would include 2 positions within same line, 
form zero to added line length. Actually `addedMessage.length - 1` is not required, `0` would be fine also.

For command (2) things bit more complicated, but in our custom `VersionsCodeLensProvider` 
it would be just referenced as `extension.fixVersion`.

Command itself would be defined in main file `src/extension.ts`:

{{< highlight ts "hl_lines=1-5 12-14 16" >}}
import * as vscode from 'vscode';
import {
	ExtensionContext,
	languages
} from "vscode";

export function activate(context: ExtensionContext) {
	console.log('Congratulations, your extension "version-me" is now active!');
	let disposable = vscode.commands.registerCommand('extension.helloWorld', () => {
		vscode.window.showInformationMessage('Hello World!');
	});
	let fixCommand = vscode.commands.registerCommand('extension.fixVersion', () =>
		vscode.window.showInformationMessage('extension.fixVersion')
	);
	context.subscriptions.push(disposable);
	context.subscriptions.push(fixCommand);
}
{{</ highlight>}}

For now our newly added command would just show message as bootstrapped code is doing:
`vscode.window.showInformationMessage('extension.fixVersion')`.

Also register our custom `VersionsCodeLensProvider` to be running for 
selection of languages with `languages.registerCodeLensProvider`.

See first parameter:
{{< highlight ts>}}
{
    language: "json",
    scheme: "file"
}
{{</ highlight>}}

It defines that `VersionsCodeLensProvider` would be running for files with `json` language.

{{< highlight ts "hl_lines=6 13-20" >}}
import * as vscode from 'vscode';
import {
	ExtensionContext,
	languages
} from "vscode";
import { VersionsCodeLensProvider } from './VersionsCodeLensProvider'

export function activate(context: ExtensionContext) {
	let fixCommand = vscode.commands.registerCommand('extension.fixVersion', () =>
		vscode.window.showInformationMessage('extension.fixVersion')
	);

	// Register our CodeLens provider
	let codeLensProvider = languages.registerCodeLensProvider(
		{
			language: "json",
			scheme: "file"
		},
		new VersionsCodeLensProvider()
	);
	context.subscriptions.push(fixCommand);
	context.subscriptions.push(codeLensProvider);
}
{{</ highlight>}}

`languages` is a helper provided by VSCode to work with widely known language sets and provide easy ways to 
attach various actions per language file. Read more [here](https://code.visualstudio.com/api/language-extensions/programmatic-language-features)

> Pay attention that `extension.helloWorld` is removed on this step.
> Together with `context.subscriptions.push`

One important thing to not forget. Each extension is bound to specific `event` happening within VSCode.
To specify this event, there is special section `activationEvents` in `package.json`:

{{< highlight json >}}
{
    "activationEvents": [
		"onLanguage:json"
	],
	"contributes": {
		"commands": [{
			"command": "extension.fixVersion",
			"title": "Fix outdated version"
		}]
	},
}
{{</ highlight>}}
_`contributes` section is also updated to reflect our newly added command. More info [here](https://code.visualstudio.com/api/references/contribution-points)_

Run our extension: `Shift+Cmd+D`, `Ctrl+F5` (run)

When opening any `.json` file extra line `Added line` should appear everywhere:

![Added line](/images/vscode-extension/added_line.png)

### Process only useful lines

_To see results of this section without typing in: `git checkout 3_show_versions`_

So far extension adds additional lines everywhere. Next step would be to add lenses only for lines needed
and also parse from somewhere current package versions.

Within `src/VersionsCodeLensProvider.ts` added 2 regular expressions to track current active line 
within file and enable/disable version tagging. 

Updated version of `provideCodeLenses`:
{{< highlight ts "hl_lines=5-7 11-23" >}}
provideCodeLenses(document: TextDocument): Promise<CodeLens[]> {
    let sourceCode = document.getText()
    const results: Promise<CodeLens>[] = [];
    const sourceCodeArr = sourceCode.split('\n');
    let regexStart = /.*(devDependencies|dependencies).*/
    let regexEnd = /},?/
    let isActive = false;

    for (let lineNum = 0; lineNum < sourceCodeArr.length; lineNum++) {
        const line = sourceCodeArr[lineNum];
        const matchStart = line.match(regexStart);
        const matchEnd = line.match(regexEnd);
        // on
        if (matchStart && matchStart.index !== undefined) {
            isActive = true;
            continue;
        }
        // off
        if (matchEnd && matchEnd.index !== undefined) {
            isActive = false;
            continue;
        }
        if (isActive) {
            results.push(this.getCodeLenseForLine(lineNum, line));
        }
    }
    return Promise.all(results);
}
{{</ highlight>}}

Run our extension: `Shift+Cmd+D`, `Ctrl+F5`

Results are as expected (after few bug fixes):
![only expected](/images/vscode-extension/only_expected.png)

### Display recent version

Now, lets retrieve most resent version. For this I would use npm package [`package-json`](https://github.com/sindresorhus/package-json)
It is doing what actually needed:

> Get metadata of a package from the npm registry 

One of the fields in metadata is "version". What actually would be used.
Internally package using npmjs registry public [API](https://github.com/npm/registry/blob/master/docs/REGISTRY-API.md)

Updated `getCodeLenseForLine` in `src/VersionsCodeLensProvider.ts` will looks like:

{{< highlight ts "hl_lines=2-9" >}}
 getCodeLenseForLine(lineNum: number, line: string): Promise<CodeLens> {
    const parts = line
        .split(':')
        .map(i => i.trim().replace(/["\^]/g, ''));
    const packageName = parts[0];

    return packageJson(packageName)
        .then(result => {
            let recentPackageVersion = result.version as string;
            let range = new Range(
                new Position(lineNum, 0),
                new Position(lineNum, recentPackageVersion.length - 1)
            )
            let c: Command = {
                command: "extension.fixVersion",
                title: recentPackageVersion,
            };
            return new CodeLens(range, c);
        })
}
{{</ highlight>}}

Run our extension: `Shift+Cmd+D`, `Ctrl+F5`

Now versions are shown after some short delay:
![versions](/images/vscode-extension/versions.png)
_It shows most recent versions on packages, and `@types/glob` actually has completely wrong version specified before_

After adding latest `underscore` package with version `1.9.1` things not became better:

![underscore](/images/vscode-extension/underscore.png)
_`1.9.1` is the latest version for `underscore` no need to show it_

One more helper coming to help `compareVersion`:

In `src/VersionsCodeLensProvider.ts` add one more function:
{{< highlight ts >}}
export const compareVersion = (a: string, b: string): boolean =>
    parseInt(a.replace(/\./g, '000'), 10) > parseInt(b.replace(/\./g, '000'), 10);
{{</ highlight>}}

It would take version inputs like `1.2.3` and `5.4.0` (I hope version specified is [`MAJOR.MINOR.PATCH`](https://semver.org/) based)
Replace every `.` with `000` and try to parse it as number. Then compare.

Some sample: `1.2.3` => `100020003`, `0.2.1` => `000020001`(`20001`). `100020003 > 20001 = true`

Also `getCodeLenseForLine` adapted to return `CodeLens[]` as array to not introduce `null`.
Later in lines 31-32 results are flattened. 

To use `array#flat()` ts compiler should be updated to use `es2019` in `target` and `lib`:

Updated `tsconfig.json`: 
{{< highlight json "hl_lines=4 6-8" >}}
{
	"compilerOptions": {
		"module": "commonjs",
		"target": "es2019",
		"outDir": "out",
		"lib": [
			"es2019"
		],
		"sourceMap": true,
		"rootDir": "src",
		"strict": true
	},
	"exclude": [
		"node_modules",
		".vscode-test"
	]
}
{{</ highlight>}}

`src/VersionsCodeLensProvider.ts` after:
{{< highlight ts "hl_lines=1-2 7 31-32 39 43-45 55 57" >}}
export const compareVersion = (a: string, b: string): boolean =>
    parseInt(a.replace(/\./g, '000'), 10) > parseInt(b.replace(/\./g, '000'), 10);

export class VersionsCodeLensProvider implements CodeLensProvider {
    provideCodeLenses(document: TextDocument): Promise<CodeLens[]> {
        let sourceCode = document.getText()
        const results: Promise<CodeLens[]>[] = [];
        const sourceCodeArr = sourceCode.split('\n');
        let regexStart = /.*(devDependencies|dependencies).*/
        let regexEnd = /},?/
        let isActive = false;

        for (let lineNum = 0; lineNum < sourceCodeArr.length; lineNum++) {
            const line = sourceCodeArr[lineNum];
            const matchStart = line.match(regexStart);
            const matchEnd = line.match(regexEnd);
            // on
            if (matchStart && matchStart.index !== undefined) {
                isActive = true;
                continue;
            }
            // off
            if (matchEnd && matchEnd.index !== undefined) {
                isActive = false;
                continue;
            }
            if (isActive) {
                results.push(this.getCodeLenseForLine(lineNum, line));
            }
        }
        return Promise.all(results)
            .then(results => results.flat());
    }

    getCodeLenseForLine(lineNum: number, line: string): Promise<CodeLens[]> {
        const parts = line
            .split(':')
            .map(i => i.trim().replace(/["\^]/g, ''));
        const [packageName, packageVersion] = parts;

        return packageJson(packageName)
            .then(result => {
                let recentPackageVersion = result.version as string;
                const isRecentBigger = compareVersion(recentPackageVersion, packageVersion)
                const isCurrentBigger = compareVersion(packageVersion, recentPackageVersion)
                if (isRecentBigger || isCurrentBigger) {
                    let range = new Range(
                        new Position(lineNum, 0),
                        new Position(lineNum, recentPackageVersion.length - 1)
                    )
                    let c: Command = {
                        command: "extension.fixVersion",
                        title: recentPackageVersion,
                    };
                    return [new CodeLens(range, c)];
                }
                return [];
            })
    }
}
{{</ highlight>}}

Run our extension: `Shift+Cmd+D`, `Ctrl+F5`

Looks good, `@types/glob` is marked for changes, and `underscore` is not :
![correct_versions_only](/images/vscode-extension/correct_versions_only.png)

All good, but when clicking version, only dummy message is shown

### Applying Version fix

_To see results of this section without typing in: `git checkout 4_apply_with_command`_

In `src/extension.ts` add function `fixVersion` and use it in `extension.fixVersion` command:

{{< highlight ts "hl_lines=10 23-38" >}}
import * as vscode from 'vscode';
import {
	ExtensionContext,
	languages,
	window
} from "vscode";
import { VersionsCodeLensProvider } from './VersionsCodeLensProvider'

export function activate(context: ExtensionContext) {
	let fixCommand = vscode.commands.registerCommand('extension.fixVersion', fixVersion);

	// Register our CodeLens provider
	let codeLensProvider = languages.registerCodeLensProvider(
		{
			language: "json",
			scheme: "file"
		},
		new VersionsCodeLensProvider()
	);
	context.subscriptions.push(fixCommand);
	context.subscriptions.push(codeLensProvider);
}
// [line number, full line, correct version]
export function fixVersion(...args: string[]) {
	const lineNum = parseInt(args[0], 10);
	const line = args[1];
	const correctVersion = args[2];
    
	let range = new vscode.Range(
		new vscode.Position(lineNum, line.lastIndexOf(':') + 1),
		new vscode.Position(lineNum, line.length)
	);
	let comma = line.indexOf(',') > -1 ? ',' : '';
	let snippet = new vscode.SnippetString(` "${correctVersion}"${comma}`);
	if (window.activeTextEditor) {
		window.activeTextEditor.insertSnippet(snippet, range);
	}
}
{{</ highlight>}}
_Lets pretend that nobody would execute command as standalone, anyway it would fail in this case_
Function would accept line number to perform update, full line and correct version which should be set.

Then at position `line.lastIndexOf(':') + 1` till the end of line, new `snippet` would be inserted, containing 
updated version. There is also logic to keep `comma` in place. When new snippet is ready it is inserted in `window.activeTextEditor`
at a position of version specification (after `:`).

Run extension last time: `Shift+Cmd+D`, `Ctrl+F5`

Click some version hints and it would replace outdated ones:
![result](/images/vscode-extension/result.png)

_Switch back to `master` to see final codebase: `git checkout master`_

# Conclusion

Extension is not production ready, please look for `version lense` or other search terms at VSCode marketplace 
if you need something to check for recent version. Currently developed extension intended for learning and 
trying things.

I think developing VSCode extension is easy enough when starting with official documentation.
Plenty of examples and practices could be found there. This would enable developers to create extensions 
for their own particular use cases and ease day by day activities by introducing additional automation
within their favorite IDE VSCode.
